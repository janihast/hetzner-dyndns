# Description

Script fetches current ip and updates/creates dns record for zone to Hetzner

# Usage

```sh
$ docker run --rm --env API_TOKEN=<hetzner api token> --env ZONE_NAME=<domain> registry.gitlab.com/janihast/hetzner-dyndns:<COMMIT HASH>
```
