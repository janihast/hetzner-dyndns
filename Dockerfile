FROM alpine:3.17.0

RUN apk add bash curl jq

COPY update.sh /bin/update.sh

ENTRYPOINT ["/bin/update.sh"]
