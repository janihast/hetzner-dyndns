#!/usr/bin/env bash

set -e

if [[ -z "${TTL}" ]]; then
	TTL=60
fi

if [[ -z "${API_TOKEN}" ]]; then
	echo "ERROR: missing api token"
	exit 1
fi

if [[ -z "${ZONE_NAME}" ]]; then
	echo "ERROR: missing zone name"
	exit 1
fi

IP4=$(curl -s4 https://ifconfig.me/ip)
if [[ "$?" != "0" ]]; then
	echo "ERROR: unable to fetch ip"
	exit 1
fi

ZONE_ID=$(curl -s "https://dns.hetzner.com/api/v1/zones?name=${ZONE_NAME}" -H "Auth-API-Token: ${API_TOKEN}" | jq -r ".zones[0].id")
if [[ "$?" != "0" ]]; then
	echo "ERROR: unable to fetch records"
	exit 1
fi

if [[ -z "${ZONE_ID}" ]]; then
	echo "ERROR: zone id not found"
	exit 1
fi

RECORD=$(curl -s "https://dns.hetzner.com/api/v1/records?zone_id=${ZONE_ID}" -H "Auth-API-Token: ${API_TOKEN}" | jq -r '.records[] | select(.type | test("^A$")) | select(.name | test("^@$"))')
if [[ "$?" != "0" ]]; then
	echo "ERROR: unable to fetch records"
fi

RECORD_ID=$(echo "${RECORD}" | jq -r .id)
RECORD_VALUE=$(echo "${RECORD}" | jq -r .value)

if [[ "${IP4}" == "${RECORD_VALUE}" ]]; then
	echo "Already up to date"
else
	DATA="{
		\"zone_id\": \"${ZONE_ID}\",
		\"type\": \"A\",
		\"name\": \"@\",
		\"value\": \"${IP4}\",
		\"ttl\": ${TTL}
	}"

	if [[ -z "${RECORD_ID}" ]]; then
		curl -s -X "POST" "https://dns.hetzner.com/api/v1/records" \
		     -H "Content-Type: application/json" \
		     -H "Auth-API-Token: ${API_TOKEN}" \
		     -d $"${DATA}" > /dev/null
	else
		curl -s -X "PUT" "https://dns.hetzner.com/api/v1/records/${RECORD_ID}" \
		     -H "Content-Type: application/json" \
		     -H "Auth-API-Token: ${API_TOKEN}" \
		     -d $"${DATA}" > /dev/null
	fi

	if [[ "$?" != "0" ]]; then
		echo "ERROR: Unable to modify record"
		exit 1
	fi

	echo "Updated"
fi

